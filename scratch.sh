#!/bin/bash

#
# scratch.sh
#
# - Initialisation d'un environnement de developpement web 
#   pour un projet `from scratch`
#  

echo "
Configuration d'un environnement de developpement web
‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒"

# controle du fichier de config
source api/config.ini.sh
source config.ini

# Saisie du nom du projet web
echo ""
echo "Préparation l'environnement de développement web"
read -p "Nom du projet web (exemple: monprojet): " siteName

if [[ "$siteName" == "" ]]; then

  echo "ERROR: Fin du script (raison: le nom du projet est vide)"
  exit 1

fi

# création du virtualhost
source api/vhost.sh $siteName $hostPath

# création des dossiers sources
www="$phpDir/$siteName/www"
logs="$phpDir/$siteName/logs"
db="$phpDir/$siteName/db"
source api/mkdir.sh $www $logs $db

# création du fichier de `.conf`
source api/ensite.sh $siteName $www $logs $confPath

