# Scripts de configuration pour développement web
Ce dépôt regroupe des scripts permettant d'initialiser un environnement de developpement web en local via lignes de commandes.

## Rendre les scripts executable
1. Lancer le terminal dans le dossier contenant les scripts (clic droit puis Ouvrir dans le terminal)
2. Depuis le terminal, lancer la commande `chmod +x ./<script>.sh && ./<script>.sh` en remplacant `<script>` par le chemin du script 
exemple : `chmod +x ./scratch.sh && ./scratch.sh`

## Liste des scripts

### Chaînes de scripts
Les chaînes de scripts permettent l'enchainement des scripts unitaires pour une configuration de bout en bout d'un environnement de développement

| Script      | Description                                                                                     |
|-------------|-------------------------------------------------------------------------------------------------|
| scratch     | Création d'un environnement de developpement vide (pour des projets *from sctrach* par exemple)  |
| translucide | Création d'un environnement de developpement sous le CMS Translucide                            |

### Scripts unitaires
Le scripts unitaires sont appelés dans les chaines mais peuvent également être appelé unitairement avec des paramètres (spécifiés dans les commentaires des scripts)

| Script                  | Description                                                               | 
|-------------------------|---------------------------------------------------------------------------|
| config.ini              | Configuration du fichier config.ini                                       |
| vhost                   | Configuration du virtualhost dans le fichier host                         |
| mkdir                   | Création des dossiers cibles de l'environnement de développement          |
| ensite                  | Création du fichier de configuration (.conf) pour le vhost et activation  |
| translucide.clone       | Git clone du CMS translucide dans le dossier du projet                    |
| translucide.theme.copy  | Copie du theme default vers le thème du projet                            |
| translucide.theme.push  | Création d'un dépot git pour le theme du projet                           |
| translucide.theme.clone | Clone d'un thème existant depuis un dépôt Git                             |  
