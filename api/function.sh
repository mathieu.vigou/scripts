#!/bin/bash

#
# fonctions des scripts
#

# Implémente une variable dans un fichier
# params : 
#  - $1 : nom de la variable
#  - $2 : valeur de la variable
#  - $3 : fichier cible
function initVar () {
cat << EOF >> "$3"
$1="$2"
EOF
}