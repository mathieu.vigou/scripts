
#!/bin/bash

#
# Git clone du CMS translucide
#
# exécution unitaire : oui
# commande: ./api/translucide.clone.sh <www>
# params : 
#  - $1 : Dossier d'installation des sites (exemple: /var/www/html/monprojet/www)
#

echo ""
echo "Clone du dépôt git de Translucide"

if [ ! -d "$1/.git" ]; then

  git clone https://github.com/simonvdfr/Translucide.git $1
  git config --add safe.directory $1/.git

  #/mnt/AE783E25783DED29/00_WEBDEV/test/www/.git/config
  
  echo "Configuration du nom et du mail (git config user.*) :"
  read -p " - user.name (exemple: Prénom) ? " name
  read -p " - user.email (exemple : mon.mail@exemple.com) ? " email
  cd $1/.git
  git config user.name "$name" --file
  git config user.email "$email"

  echo "SUCCESS: Le projet git a correctement été cloné"

else

  echo "WARNING: Le projet contient déjà un dépot git"

fi