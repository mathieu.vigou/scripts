#!/bin/bash

#
# Création d'un dépot git pour le theme
#
# exécution unitaire : oui
# commande: ./api/translucide.theme.push.sh <themePath> <www> <repo>
# params : 
#  - $1 : Nom du site (exemple: monprojet)
#  - $2 : Dossier d'installation des sites (exemple: /var/www/html/monprojet/www)
#  - $3 : Repo distant sur lequel on push le theme (exemple: https://framagit.org/groupe/projet)
#

if [ ! -d "$2/theme/$1/.git" ]; then

    echo "./api/translucide.theme.push.sh $1 $2 $3"

    echo ""
    echo "Configuration du nom et du mail (git config user.*) :"
    read -p " - user.name (exemple: Prénom) ? " name
    read -p " - user.email (exemple : mon.mail@exemple.com) ? " email

    # git init
    cd "$2/theme/$1"
    git init

    #git config --global --add safe.directory ./
    git config user.name "$name"
    git config user.email "$email"
    git config credential.helper store

    #initialisation du dépot
    git remote add origin $3/$1.git 
    git branch -M main
    git add .
    git commit -m "🎉 initialisation du theme avec le theme default"
    git push -u origin main

    # création d'une branche de demonstration
    git branch staging main 
    git checkout staging
    git push -u origin staging

    # création d'une branche de developpement
    git branch develop main 
    git checkout develop
    git push -u origin develop

    echo "SUCCESS: Le projet git a correctement été cloné"

else

    echo "WARNING: Le projet contient déjà un dépot git"

fi