#!/bin/bash

#
# Configuration du virtualhost dans le fichier "hosts"
#
# exécution unitaire : oui
# commande: ./api/vhost.sh <siteName> <hostPath>
# params : 
#  - $1 : Nom du site (exemple: monprojet)
#  - $2 : Dossier d'installation des sites (exemple: /var/www/html)
#

echo ""
echo "Configuration du virtualhost"
if grep -q -wi "$1" $2; then

echo "INFO: virtualhost \"$1\" est déjà configuré dans \"$2\""

else

sudo chmod 777 $2 -R
cat << EOF >> $2
127.0.0.1	$1.local www.$1.local
EOF
echo "SUCCESS: le virtualhost \"$1.local\" a correctement été configuré"

fi