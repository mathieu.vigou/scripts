#!/bin/bash

#
# Copie du theme default vers le thème du projet
#
# exécution unitaire : oui
# commande: ./api/translucide.theme.copy.sh <siteName> <www>
# params : 
#  - $1 : Nom du site (exemple: monprojet)
#  - $2 : Dossier d'installation des sites (exemple: /var/www/html/monprojet/www)
#

echo ""
echo "Copie du theme \"default\" vers \"$1\""
cp -r "$2/theme/default" "$2/theme/$1"
echo "SUCCESS: Le thème a été copié"