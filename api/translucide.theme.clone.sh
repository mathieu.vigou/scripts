#!/bin/bash

#
# Clone d'un thème existant depuis un dépôt Git
#
# exécution unitaire : oui
# commande: ./api/translucide.theme.clone.sh <siteName> <www>
# params : 
#  - $1 : Nom du site (exemple: monprojet)
#  - $2 : Dossier d'installation des sites (exemple: /var/www/html/monprojet/www)
#

echo ""
echo "Clone du dépot git existant"
read -p "url HTTPS du dépot (exemple : https://framagit.org/projet/depot.git) : " repo

# clone
git clone $repo "$2/theme/$1"

# git config
cd $1
echo "Configuration du nom et du mail (git config user.*) :"
read -p " - user.name (exemple: Prénom) ? " name
read -p " - user.email (exemple : mon.mail@exemple.com) ? " email
git config user.name "$name"
git config user.email "$email"
git config credential.helper store

echo "SUCCESS: Le projet git a correctement été cloné"
