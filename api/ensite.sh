#!/bin/bash

#
# Création du fichier de configuration (.conf) pour le vhost
# et activation de la configuration
#
# exécution unitaire : oui
# commande: ./api/ensite.sh <siteName> <wwwDir> <logsDir> <confPath>
# params : 
#  - $1 : Nom du site (exemple: monprojet)
#  - $2 : Dossier des sources (exemple: /var/www/html/monprojet/www)
#  - $3 : Dossier des logs (exemple: /var/www/html/monprojet/logs)
#  - $4 : Chemin des fichiers de configuration des vhosts (exemple: /etc/apache2/sites-available)

echo ""
echo "Configuration du fichier \"$1.local.conf\""

if [ -e "$4/$1.local.conf" ]; then

echo "WARNING: le fichier de configuration du vhost existe déjà"

else

sudo chmod 777 $4 -R
cat << EOF > "$4/$1.local.conf"
<VirtualHost *:80>
    ServerName $1.local
    Serveralias www.$1.local
    DocumentRoot "$2"
    <directory $2>
        Order allow,deny
        Allow from all
        Require all granted
    </directory>
    CustomLog $3/access.log common
    ErrorLog $3/error.log
</VirtualHost>
EOF
echo "SUCCESS: Le fichier de configuration du vhost a correctement été créé"

sudo a2ensite $1.local.conf #activation de la configuration
systemctl reload apache2 #relance d'apache

fi