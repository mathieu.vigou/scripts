#!/bin/bash

#
# Initialisation du fichier de configuration
#
# exécution unitaire ? oui
# commande : ./api/config.ini.sh
#

source api/function.sh

echo ""
if [ ! -f "config.ini" ]; then

    echo "Création du fichier de configutration"

    read -p "Chemin du fichier host (exemple: /etc/hosts) : " input
    initVar "hostPath" $input "config.ini"

    read -p "Chemin des fichiers de configuration des vhosts (exemple: /etc/apache2/sites-available) : " input
    initVar "confPath" $input "config.ini"

    read -p "Dossier d'installation des sites (exemple: /var/www/html): " input
    initVar "phpDir" $input "config.ini"

    echo "SUCCESS: Le fichier de configuration a été correctement crée"

else

    read -p "Controler le fichier \""config.ini"\" avant de continuer [o/n] ? " opt

    case $opt in

        "o")
        nano "config.ini"
        ;;

        "n")
        ;;

        *) 
        echo "ERREUR: Fin du script (raison: option \"$opt\" invalide)"
        exit 1
        ;;

    esac

fi