#!/bin/bash

#
# Création des dossiers cibles de l'environnement de développement
#
# exécution unitaire : oui
# commande: ./api/mkdir.sh <www> <logs> <db>
# params : 
#  - $1 : Dossier des sources (exemple: /var/www/html/monprojet/www)
#  - $2 : Dossier des logs (exemple: /var/www/html/monprojet/logs)
#  - $3 : Dossier des sauvegarde de database (exemple: /var/www/html/monprojet/db)
#

echo ""
echo "Création des dossiers du projets"

if [ ! -d $1 ]; then 
  mkdir -p $1;
  echo "SUCCESS: Le dossier \"www\" à correctement été créé"
else
  echo "INFO: Le dossier \"www\" existe déja"
fi

if [ ! -d $2 ]; then
  mkdir -p $2;
  echo "SUCCESS: Le dossier \"logs\" à correctement été créé"
else
  echo "INFO: Le dossier \"logs\" existe déja"
fi

if [ ! -d $3 ]; then
  mkdir -p $3;
  echo "SUCCESS: Le dossier \"db\" à correctement été créé"
else
  echo "INFO: Le dossier \"db\" existe déja"
fi
