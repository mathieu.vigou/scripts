#!/bin/bash

#
# translucide.sh
#
# - Initialisation d'un environnement de developpement web 
#   sous le CMS Translucide
#  

source config.ini
curDir=$PWD

# Initialisation d'un environnement de developpement web 
source scratch.sh

echo "
Configuration du CMS Translucide
‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒"

# Clone du dépot git de Translucide
www="$phpDir/$siteName/www"
source api/translucide.clone.sh $www
cd $curDir

# Chemin du theme
echo ""
echo "Configuration du thème du projet :
 1) Copie du thème default sans git
 2) Copie et création d'un dépot git
 3) Clone d'un dépot git existant
 4) Ne rien faire"
read -p "$(echo -e Que souhaitez-vous faire ?) " opt

case $opt in
    "1")
        # copie du theme "default" vers le theme du projet
        source api/translucide.theme.copy.sh $siteName $www
        ;;
    "2")
        # copie du theme "default" vers le theme du projet
        source api/translucide.theme.copy.sh $siteName $www

        # création du dépot git
        if [[ -z $repo_theme ]]; then
            read -p "URL du dépôt Git (exemple: https://framagit.org/groupe/projet): " repo
            initVar "repo_theme" $repo "config.ini"
        else
            repo=$repo_theme
        fi
        
        source api/translucide.theme.push.sh $siteName $www $repo
        ;;
    "3")
        # clone d'un theme existant
        source api/translucide.theme.clone.sh $siteName $www
        ;;
    "4")
        break
        ;;
    *) echo "invalid option $opt";;
esac
cd $curDir

# Création de la base de donnée
mysql -u root<<MYSQL_SCRIPT
CREATE DATABASE $siteName;
GRANT ALL PRIVILEGES ON $siteName.* TO root@localhost;
FLUSH PRIVILEGES;
MYSQL_SCRIPT

# Ouverture de l'interface d'instalation 
open http://$siteName.local